const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();

const argv = require("./server").argv

const forumRoutes = require('./routes/forum');
const userRoutes = require('./routes/user');
const kidRoutes = require('./routes/kid');
const messageRoutes = require('./routes/message');

/** MONGODB CONNECTION **/
const mongoDbUri = argv.mongoDbUri
mongoose.connect(mongoDbUri,
        { useNewUrlParser: true,
                useUnifiedTopology: true })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

/** ENABLE CORS **/
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

/** BODY-PARSER USE **/
app.use(bodyParser.json());

app.use('/forum/', forumRoutes);
app.use('/user/', userRoutes);
app.use('/child/', kidRoutes);
app.use('/message/', messageRoutes);



module.exports = app;
