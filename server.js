const https = require('https');
const http = require('http');
const fs = require('fs');

// yargs to handle parameters
const yargs = require('yargs');
const argv = yargs
    .options({
            https: {
                type: 'boolean',
                default: false,
                description: "Run the server with https"
            }
        }
    )
    .options({
            httpsPassphrase: {
                type: 'string',
                alias: 'httpsP',
                default: "vivelesenfants",
                description: "Passphrase for the https certificate"
            }
        }
    )
    .options({
        mongoDbUri: {
            type: 'string',
            default: "mongodb+srv://baseUser:ProjetLinfo1212@linfo1212.hunxb.mongodb.net/myFirstDatabase",
            description: "URI for the mongodb connection"
        }
    })
    .help()
    .alias("help", "h").argv

exports.argv = argv

// app placed after yargs to have a non-circular depedency
const app = require('./app');

// port normalization
const normalizePort = val => {
    const port = parseInt(val, 10);

    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }
    return false;
};
const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

//error handler for server
const errorHandler = error => {
    if (error.syscall !== 'listen') {
        throw error;
    }
    const address = server.address();
    const bind = typeof address === 'string' ? 'pipe ' + address : 'port: ' + port;
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges.');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use.');
            process.exit(1);
            break;
        default:
            throw error;
    }
};

// server definiton controlled by parameters
let server;
if(argv.https){
    server = https.createServer({
        key: fs.readFileSync("server.key"),
        cert: fs.readFileSync("server.cert"),
        passphrase: argv.httpsPassphrase
    }, app);
} else {
    server = http.createServer(app);
}

console.info("MongoDB URI: ", argv.mongoDbUri);

// server on action handling
server.on('error', errorHandler);
server.on('listening', () => {
    const address = server.address();
    const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + port;
    console.log('Listening on ' + bind);
});

server.listen(port);
