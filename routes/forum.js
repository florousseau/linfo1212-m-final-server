const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth')

const forumCtrl = require('../controllers/forum');

router.post('/create/',   forumCtrl.createForum);
router.delete('/delete/', forumCtrl.deleteForum);
router.get('/get/', forumCtrl.getAllForums);
router.get('/get/:id', forumCtrl.getOneForum);
router.post('/search/:search', forumCtrl.searchForums);

module.exports = router;
