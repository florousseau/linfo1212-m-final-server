const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth')

const messageCtrl = require('../controllers/message');

router.post('/create/', auth, messageCtrl.createMessage);
router.delete('/delete/', auth, messageCtrl.deleteMessage);
router.get('/get/', messageCtrl.getMessage);
router.post('/search/:search', messageCtrl.searchMessage);

module.exports = router;
