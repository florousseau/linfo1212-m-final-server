const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');

const userCtrl = require('../controllers/user');

router.post('/register/', userCtrl.createUser);
router.delete('/delete/', auth, userCtrl.deleteUser);
router.post('/login/', userCtrl.loginUser);
router.post('/get/', auth, userCtrl.getUser);

module.exports = router;