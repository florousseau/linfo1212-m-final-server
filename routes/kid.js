const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');

const kidCtrl = require('../controllers/kid');

router.post('/enrol/', auth, kidCtrl.createKid);
router.delete('/delete/', auth, kidCtrl.deleteKid);
router.post('/getOne/', auth, kidCtrl.getKid);
router.post('/get/', auth, kidCtrl.getKids);

module.exports = router;
