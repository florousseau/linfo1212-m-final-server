const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    forum: {type: mongoose.ObjectId, required: true},
    date: {type: Date, default: Date.now()},
    author: {type: String, required: true},
    authorId: {type: mongoose.ObjectId, required: true},
    post: {type: String, required: true}
})

module.exports = mongoose.model('Message', messageSchema);
