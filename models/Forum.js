const mongoose = require('mongoose');

const forumSchema = new mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    postDate: {type: Date, default: Date.now()},
    author: {type: String, required: true},
    authorId: {type: mongoose.ObjectId, required: true}

});

module.exports = mongoose.model('Forum', forumSchema);



