const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const kidSchema = new mongoose.Schema({
    name: { type: String, required: true },
    firstname: { type: String, required: true },
    birthdate: {type: Date, required: true},
    team: {type: String, default: null},
    ownerId: { type: String, required: true},
    paid: { type: Boolean, default: false}
});

kidSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Kid', kidSchema);
