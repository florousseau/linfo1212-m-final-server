const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, 'RANDOM_SECRET_TOKEN');
        const userId = decodedToken.userId;
        if(!req.body._id){
            throw 'No user ID';
        } else if(req.body._id !== userId){
            throw 'Invalid user ID';
        } else {
            next();
        }
    } catch (err) {
        res.status(401).json({
            error: err || "Request unauthorized, token is invalid"
        });
    }
};
