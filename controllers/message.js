const Message = require('../models/Message');
const mongoose = require("mongoose");


exports.createMessage = (req, res) => {
    const body = req.body;
    const message = new Message({
        forum: body.message.forum,
        date: body.message.date,
        author: body.message.author,
        authorId: body._id,
        post: body.message.post
    });
    message.save()
        .then(() => res.status(201).json({message: 'Message created successfully!', messageid: message._id}))
        .catch(err => res.status(500).json({error: err}))
};

exports.deleteMessage = (req, res, next) => {
    Message.findOne({_id: req.body.messageId},)
        .then(message => {
            if (!message) {
                res.status(404).json({error: "Message not found"})
            } else {
                if(req.body._id === message.authorId.toString()){
                    /** deleting message **/
                    Message.deleteOne({_id: req.body.messageId})
                        .then(() => res.status(200).json({message: "Message deleted"}))
                        /** catching error from deleting **/
                        .catch(err => res.status(500).json({error: err}))
                } else {
                    throw "Invalid user for this message"
                }
            }
            /** catching error during search **/
        }).catch(err => res.status(500).json({error: err}))
};

exports.getMessage = (req, res, next) => {
    Message.findOne({_id: req.body.messageId})
        .then(forum => {
            Message.find({_id: req.body.messageId})
                .then(message => res.status(200).json(message))
        })
        .catch(err => res.status(404).json({error: err}));
}

exports.searchMessage = (req, res, next) => {
    const searchWord = req.params.search.toLowerCase();
    Message.aggregate([
        {
            $match: {
                $or: [
                    {author: {$regex: searchWord, $options: 'i'}},
                    {post: {$regex: searchWord, $options: 'i'}}
                ]
            }
        }
    ])
        .then(messages => {
            res.status(200).json({results: Object.assign({}, messages)})
        })
        .catch(err => {
            res.status(500).json({error: err})
        })
}
