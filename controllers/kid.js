const Kid = require('../models/Kid')
const User = require("../models/User");

exports.createKid = (req, res, next) => {
    const kidObject = req.body.kid;
    /** kid creation **/
    const kid = new Kid({
        name: kidObject.name,
        firstname: kidObject.firstname,
        birthdate: kidObject.birthdate,
        ownerId: kidObject.ownerId,
        paid: false

    });
    /** kid saving **/
    kid.save()
        .then(() => {
            User.updateOne({_id: req.body.kid.ownerId}, {
                $push: {
                    kids: {
                        kidId: kid._id
                    }
                }
            })
        })
            .then(() => res.status(201).json({message: 'Kid created successfully'}))
            .catch(err => res.status(400).json({error: err})) /** error catching **/
        .catch(err => res.status(400).json({error: err})) /** error catching **/

}

exports.deleteKid = (req, res, next) => {
    /** kid search **/
    Kid.findOne({_id: req.body.kidId},)
        .then(kid => {
            if (!kid) {
                res.status(401).json({error: "Kid not found"})
            }
            /** deleting kid **/
            Kid.deleteOne({_id: req.body.kidId})
                .then(() => res.status(200).json({message: "Kid deleted"}))
                /** catching error from deleting **/
                .catch(err => res.status(500).json({error: err}))
            /** catching error during search **/
        }).catch(err => res.status(500).json({error: err}))
};


exports.getKid = (req, res, next) => {
    Kid.findOne({_id: req.body.kidId})
        .then(kid => {
            if (!kid) {
                return res.status(404).json({error: "Kid not found"})
            }
            res.status(200).json({
                name: kid.name,
                surname: kid.surname,
                birth: kid.birth,
                team: kid.team,
            })
        })
        .catch(err => res.status(500).json({error: err}))
};

exports.getKids = (req, res, next) => {

    Kid.find({ownerId: req.body._id})
        .then(kids => {
            res.status(200).json(kids)
        })
        .catch(err => res.status(500).json({error: err}))
};
