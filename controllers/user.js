const User = require('../models/User')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const privateKey = "RANDOM_SECRET_TOKEN"

exports.createUser = (req, res, next) => {
    const userObject = req.body.user;
    bcrypt.hash(userObject.password, 10)
        /** password hash **/
        .then(hash => {
            /** user creation **/
            const user = new User({
                name: userObject.name,
                firstname: userObject.firstname,
                email: userObject.email,
                phone : userObject.phone,
                password: hash
            });
            /** user saving **/
            user.save()
                .then(() => res.status(201).json({message: 'User created successfully'}))
                /** error catching **/
                .catch(err => {
                    res.status(400).json({error: err})
                })
            /** error from hash catching **/
        }).catch(err => res.status(500).json({error: err}));
}

exports.deleteUser = (req, res, next) => {
    /** user search **/
    User.findOne({_id: req.body.userId},)
        .then(user => {
            if (!user) {
                res.status(401).json({error: "User not found"})
            }
            /** deleting user **/
            User.deleteOne({_id: req.body.userId})
                .then(() => res.status(200).json({message: "User deleted"}))
                /** catching error from deleting **/
                .catch(err => res.status(500).json({error: err}))
            /** catching error during search **/
        }).catch(err => res.status(500).json({error: err}))
};

exports.loginUser = (req, res, next) => {
    User.findOne({email: req.body.email})
        .then(user => {
            if (!user) {
                return res.status(401).json({error: "User not found or password is incorrect"});
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({error: "User not found or password is incorrect"});
                    }
                    res.status(200).json({
                        _id: user._id,
                        token: jwt.sign(
                            {userId: user._id},
                            privateKey,
                            {expiresIn: "48h"}
                        )
                    })
                })
                .catch(err => res.status(500).json({error: err}))
        })
        .catch(err => res.status(500).json({error: err}))
};

exports.getUser = (req, res, next) => {
    User.findOne({_id: req.body._id})
        .then(user => {
            if(!user){
                return res.status(404).json({error: "User not found"})
            }
            res.status(200).json({
                firstname: user.firstname,
                name: user.name,
                email: user.email,
                phone: user.phone,
            })
        })
        .catch(err => res.status(500).json({error: err}))
};
