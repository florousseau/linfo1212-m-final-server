const Forum = require('../models/Forum');
const Message = require('../models/Message');
const mongoose = require("mongoose");

exports.createForum = (req, res) => {
    const body = req.body;
    const forum = new Forum({
        title: body.forum.title,
        description: body.forum.description,
        postDate: body.forum.postDate,
        author: body.forum.author,
        authorId: body._id
    });
    forum.save()
        .then(() => res.status(201).json({message: 'Forum created successfully!', forumId: forum._id}))
        .catch(err => res.status(500).json({error: err}))
};

exports.deleteForum = (req, res) => {
    Forum.findOne({_id: req.body.forumId},)
        .then(forum => {
            if (!forum) {
                res.status(404).json({error: "Forum not found"})
            } else {
                if(req.body._id === forum.authorId.toString()){
                    /** deleting forum **/
                    Forum.deleteOne({_id: req.body.forumId})
                        .then(() => res.status(200).json({message: "Forum deleted"}))
                        /** catching error from deleting **/
                        .catch(err => res.status(500).json({error: err}))
                } else {
                    throw "Invalid user id for this forum"
                }
            }
            /** catching error during search **/
        }).catch(err => res.status(500).json({error: err}))
};

exports.getAllForums = (req, res) => {
    Forum.find()
        .then(forums => res.status(200).json(forums))
        .catch(err => res.status(404).json({error: err}));
}

exports.getOneForum = (req, res) => {
    const id = req.params.id
    Forum.findOne({_id: id})
        .then(forum => {
            Message.find({forum: id})
                .then(messages => res.status(200).json({
                    id: forum._id,
                    title: forum.title,
                    postDate: forum.postDate,
                    description: forum.description,
                    author: forum.author,
                    authorId: forum.authorId,
                    messages: messages
                }))
                .catch(err => res.status(404).json({error: "Forum not found"}));
        })
        .catch(err => res.status(404).json({error: "Forum not found"}));
}

exports.searchForums = (req, res) => {
    const searchWord = req.params.toLowerCase();
    Forum.aggregate([
        {
            $match: {
                $or: [
                    {description: {$regex: searchWord, $options: 'i'}},
                    {title: {$regex: searchWord, $options: 'i'}},
                    {author: {$regex: searchWord, $options: 'i'}}
                ]
            }
        }
    ])
        .then(forums => {
            res.status(200).json({results: Object.assign({}, forums)})
        })
        .catch(err => {
            res.status(500).json({error: err})
        })
}
